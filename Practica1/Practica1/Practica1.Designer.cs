﻿namespace Practica1
{
    partial class Practica1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnCalcular = new Button();
            textboxValor1 = new TextBox();
            textboxValor2 = new TextBox();
            comboOperacion = new ComboBox();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            labelResultado = new Label();
            SuspendLayout();
            // 
            // btnCalcular
            // 
            btnCalcular.Location = new Point(170, 203);
            btnCalcular.Name = "btnCalcular";
            btnCalcular.Size = new Size(121, 23);
            btnCalcular.TabIndex = 0;
            btnCalcular.Text = "CALCULAR";
            btnCalcular.UseVisualStyleBackColor = true;
            btnCalcular.Click += btnCalcular_Click;
            // 
            // textboxValor1
            // 
            textboxValor1.Location = new Point(170, 104);
            textboxValor1.Name = "textboxValor1";
            textboxValor1.Size = new Size(121, 23);
            textboxValor1.TabIndex = 1;
            // 
            // textboxValor2
            // 
            textboxValor2.Location = new Point(170, 150);
            textboxValor2.Name = "textboxValor2";
            textboxValor2.Size = new Size(121, 23);
            textboxValor2.TabIndex = 2;
            // 
            // comboOperacion
            // 
            comboOperacion.DropDownStyle = ComboBoxStyle.DropDownList;
            comboOperacion.FormattingEnabled = true;
            comboOperacion.Items.AddRange(new object[] { "Sumar", "Restar", "Dividir", "Multiplicar" });
            comboOperacion.Location = new Point(170, 63);
            comboOperacion.Name = "comboOperacion";
            comboOperacion.Size = new Size(121, 23);
            comboOperacion.TabIndex = 3;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(130, 45);
            label1.Name = "label1";
            label1.Size = new Size(62, 15);
            label1.TabIndex = 4;
            label1.Text = "Operacion";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(126, 107);
            label2.Name = "label2";
            label2.Size = new Size(45, 15);
            label2.TabIndex = 5;
            label2.Text = "Valor 1:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(126, 158);
            label3.Name = "label3";
            label3.Size = new Size(42, 15);
            label3.TabIndex = 6;
            label3.Text = "Valor 2";
            // 
            // labelResultado
            // 
            labelResultado.AutoSize = true;
            labelResultado.Location = new Point(130, 243);
            labelResultado.Name = "labelResultado";
            labelResultado.Size = new Size(65, 15);
            labelResultado.TabIndex = 7;
            labelResultado.Text = "Resultado: ";
            // 
            // Practica1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(437, 318);
            Controls.Add(labelResultado);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(comboOperacion);
            Controls.Add(textboxValor2);
            Controls.Add(textboxValor1);
            Controls.Add(btnCalcular);
            Name = "Practica1";
            Text = "Practica1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnCalcular;
        private TextBox textboxValor1;
        private TextBox textboxValor2;
        private ComboBox comboOperacion;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label labelResultado;
    }
}