﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1
{
    public partial class Practica1 : Form
    {
        public Practica1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            // Obtener la operacion
            string operacion = comboOperacion.Text;

            if (operacion.Length > 0 )
            {
                int Resultado = 0;

                int Valor1 = int.Parse(textboxValor1.Text);
                int Valor2 = int.Parse(textboxValor2.Text);
                          
                if (operacion == "Sumar")
                {
                    Resultado = Valor1 + Valor2;
                } else if(operacion == "Restar")
                {
                    Resultado = Valor1 - Valor2;
                }
                else if (operacion == "Dividir")
                {
                    Resultado = Valor1 / Valor2;
                }
                else if (operacion == "Multiplicar")
                {
                    Resultado = Valor1 * Valor2;
                }

                labelResultado.Text = "Resultado: " + Resultado.ToString();
            }


        }
    }
}
