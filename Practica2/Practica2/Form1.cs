namespace Practica2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            string operacion = comboOperacion.Text;
            string valorText1 = textboxValor1.Text;
            string valorText2 = textboxValor2.Text;

            if (string.IsNullOrEmpty(operacion))
            {
                MessageBox.Show("Debe seleccionar una operacion");
                return;
            }

            if (string.IsNullOrEmpty(valorText1))
            {
                MessageBox.Show("Debe indicar el valor 1");
                return;
            }

            if (string.IsNullOrEmpty(valorText2))
            {
                MessageBox.Show("Debe indicar el valor 2");
                return;
            }

            int Resultado = 0;

            int Valor1 = int.Parse(valorText1);
            int Valor2 = int.Parse(valorText2);

            if (operacion == "Sumar")
            {
                Resultado = Valor1 + Valor2;
            }
            else if (operacion == "Restar")
            {
                Resultado = Valor1 - Valor2;
            }
            else if (operacion == "Dividir")
            {
                Resultado = Valor1 / Valor2;
            }
            else if (operacion == "Multiplicar")
            {
                Resultado = Valor1 * Valor2;
            }

            textboxResultado.ReadOnly = false;

            textboxResultado.Text = Resultado.ToString();

            textboxResultado.ReadOnly = true;

        }
    }
}