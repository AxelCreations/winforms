﻿namespace Practica2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            labelResultado = new Label();
            label3 = new Label();
            label2 = new Label();
            label1 = new Label();
            comboOperacion = new ComboBox();
            textboxValor2 = new TextBox();
            textboxValor1 = new TextBox();
            btnCalcular = new Button();
            textboxResultado = new TextBox();
            SuspendLayout();
            // 
            // labelResultado
            // 
            labelResultado.AutoSize = true;
            labelResultado.Location = new Point(128, 215);
            labelResultado.Name = "labelResultado";
            labelResultado.Size = new Size(65, 15);
            labelResultado.TabIndex = 15;
            labelResultado.Text = "Resultado: ";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(148, 168);
            label3.Name = "label3";
            label3.Size = new Size(42, 15);
            label3.TabIndex = 14;
            label3.Text = "Valor 2";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(148, 117);
            label2.Name = "label2";
            label2.Size = new Size(45, 15);
            label2.TabIndex = 13;
            label2.Text = "Valor 1:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(152, 55);
            label1.Name = "label1";
            label1.Size = new Size(62, 15);
            label1.TabIndex = 12;
            label1.Text = "Operacion";
            // 
            // comboOperacion
            // 
            comboOperacion.DropDownStyle = ComboBoxStyle.DropDownList;
            comboOperacion.FormattingEnabled = true;
            comboOperacion.Items.AddRange(new object[] { "Sumar", "Restar", "Dividir", "Multiplicar" });
            comboOperacion.Location = new Point(192, 73);
            comboOperacion.Name = "comboOperacion";
            comboOperacion.Size = new Size(121, 23);
            comboOperacion.TabIndex = 11;
            // 
            // textboxValor2
            // 
            textboxValor2.Location = new Point(192, 160);
            textboxValor2.Name = "textboxValor2";
            textboxValor2.Size = new Size(121, 23);
            textboxValor2.TabIndex = 10;
            // 
            // textboxValor1
            // 
            textboxValor1.Location = new Point(192, 114);
            textboxValor1.Name = "textboxValor1";
            textboxValor1.Size = new Size(121, 23);
            textboxValor1.TabIndex = 9;
            // 
            // btnCalcular
            // 
            btnCalcular.Location = new Point(192, 253);
            btnCalcular.Name = "btnCalcular";
            btnCalcular.Size = new Size(121, 23);
            btnCalcular.TabIndex = 8;
            btnCalcular.Text = "CALCULAR";
            btnCalcular.UseVisualStyleBackColor = true;
            btnCalcular.Click += btnCalcular_Click;
            // 
            // textboxResultado
            // 
            textboxResultado.Location = new Point(192, 210);
            textboxResultado.Name = "textboxResultado";
            textboxResultado.ReadOnly = true;
            textboxResultado.Size = new Size(121, 23);
            textboxResultado.TabIndex = 16;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(487, 359);
            Controls.Add(textboxResultado);
            Controls.Add(labelResultado);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(comboOperacion);
            Controls.Add(textboxValor2);
            Controls.Add(textboxValor1);
            Controls.Add(btnCalcular);
            Name = "Form1";
            Text = "Practica 2";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label labelResultado;
        private Label label3;
        private Label label2;
        private Label label1;
        private ComboBox comboOperacion;
        private TextBox textboxValor2;
        private TextBox textboxValor1;
        private Button btnCalcular;
        private TextBox textboxResultado;
    }
}